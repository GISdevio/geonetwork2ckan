# Setup

## Requirements

It is suggested to install the software using `pipx`:

```bash
pip3 install --user pipx
pipx install .
```

## Configuration

Customize the configuration files stored inside `conf` and `conf/sources/`.

# Execution

```bash
geonetwork2ckan
```

Common options:
- Add `ckan.apikey=$KEY` instead of saving the API key to `conf/config.yaml`
- Add `--config-path /path/to/conf/` if `conf/` is not located inside the working directory
- Add `hydra.verbose=true` to debug

Logs are available inside `outputs/`.

# Development

Instead of installing the package with `pip` or `pipx`, use `pdm`.

## Setup

```bash
pipx install pdm
```

Use of `pre-commit` is suggested.

```bash
pipx install pre-commit
pre-commit install
```

Run the application using `pdm`:
```
pdm install
pdm run ./geonetwork2ckan.py
```
