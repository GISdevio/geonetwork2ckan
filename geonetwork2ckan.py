#!/usr/bin/env python3

import collections
import logging
import urllib.parse

import hydra
import requests
import slugify
import xmltodict
from ckanapi import NotAuthorized, NotFound, RemoteCKAN
from hydra.utils import instantiate
from omegaconf import DictConfig, OmegaConf

log = logging.getLogger(__name__)


def get_last(url):
    return urllib.parse.unquote(url).rsplit("/", 1)[1]


def get_resources(dataset, name):
    results = dataset.get(name)
    if results:
        if type(results) == collections.OrderedDict:
            results = [results]
        for node in results:
            yield get_last(node["@rdf:resource"])


def fix_bad_url(page, url):
    """Many GeoNetwork instances are misconfigured and return localhost"""
    page_parsed = urllib.parse.urlparse(page)
    netloc = urllib.parse.urlparse(page).netloc
    return (
        urllib.parse.urlparse(url)
        ._replace(
            netloc=page_parsed.netloc,
            scheme=page_parsed.scheme,
        )
        .geturl()
    )


def fetch(endpoint):
    nextpage = endpoint
    while nextpage:
        log.debug(f"Fetch: {nextpage}")
        response = requests.get(nextpage)
        tree = xmltodict.parse(response.content)
        root = tree["rdf:RDF"]
        nextpage = root["hydra:PagedCollection"].get("hydra:nextPage")
        for dataset in root["dcat:Catalog"]["dcat:dataset"]:
            if "dcat:Dataset" not in dataset.keys():
                continue
            dataset = dataset["dcat:Dataset"]
            log.debug(f"Dataset found: {dataset}")
            yield {
                "name": slugify.slugify(dataset["dct:identifier"]),
                "title": dataset["dct:title"],
                "notes": dataset["dct:abstract"],
                "created": dataset.get("dct:issued"),
                "modified": dataset.get("dct:updated"),
                "category": list(get_resources(dataset, "dcat:theme")),
                "provider": list(get_resources(dataset, "dcat:publishers")),
                "resources": [
                    {
                        "name": "External dataset",
                        "url": fix_bad_url(endpoint, dataset["@rdf:about"]),
                    }
                ],
            }


def translate(mappings, result):
    for field, mapping in mappings.items():
        if field not in result:
            continue
        values = result.get(field)
        if type(values) == str:
            values = [values]
        for value in values:
            if "_default_" in mapping:
                default = mapping["_default_"]
                log.warning(f"'{value}' not found! Using {default} for field {field}")
            elif value not in mapping:
                log.warning(f"'{value}' not found! Skipping.")
                continue
            translated = mapping.get(value, default)
            log.debug(f"Replace '{value}' with '{translated}' for field {field}")
            result[field] = translated
    return result


@hydra.main(version_base=None, config_path="conf", config_name="config")
def sync(config):
    ckan = RemoteCKAN(config["ckan"]["url"], apikey=config["ckan"]["apikey"])
    for src_name, src_params in config.get("sources", {}).items():
        org_name = src_params["organization"]
        try:
            # Get organization
            org = ckan.action.organization_show(id=org_name)
        except NotFound:
            # Create organization if missing
            log.info(f"Create organization: {org_name}")
            org = ckan.action.organization_create(name=org_name)
        log.info(f"Fetch data from: {src_name}")
        mappings = src_params.get("mappings", {})
        for result in fetch(src_params["endpoint"]):
            result = translate(mappings, result)
            values = src_params.get("values", {})
            values_converted = instantiate(values, _convert_="all")
            result.update(**values_converted)
            result["owner_org"] = org["id"]
            log.debug(f"Result: {result}")
            try:
                # Get dataset
                pkg = ckan.action.package_show(id=result["name"])
                # Update dataset
                pkg.update(**result)
                log.info(f"Update dataset: {result['name']}")
                pkg = ckan.action.package_update(**result)
            except NotFound:
                # Create dataset if missing
                log.info(f"Create dataset: {result['name']}")
                pkg = ckan.action.package_create(**result)
            log.debug(f"CKAN dataset: {pkg}")


if __name__ == "__main__":
    sync()
